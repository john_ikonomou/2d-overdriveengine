#include "ODengine/IO_Manager.h"
#include "ODengine/ErrorHandling.h"

#include <fstream>
namespace OD
{
	char * IO_Manager::LoadFileToString(const char* FilePath)
	{
		FILE* file;
		fopen_s(&file, FilePath, "rb");
		if (file == nullptr)
		{
			fatalError("File Failed To Load");
			return nullptr;
		}

		fseek(file, 0, SEEK_END);
		unsigned int FileLength = ftell(file);
		fseek(file, 0, SEEK_SET);
		char* Source = new char[FileLength + 1];
		memset(Source, 0, FileLength + 1);
		fread(Source, sizeof(char), FileLength, file);
		fclose(file);

		return Source;
	}

	bool IO_Manager::LoadFileToBuffer(const char* FilePath, std::vector<unsigned char>& Buffer)
	{
		std::ifstream File(FilePath, std::ios::binary);
		if (File.fail())
		{
			perror(FilePath);
			return false;
		}

		//Seek to the end
		File.seekg(0, std::ios::end);

		//Get the file size
		int FileSize = (int)File.tellg();
		File.seekg(0, std::ios::beg);

		//Reduce file size by any header bits.
		FileSize -= (int)File.tellg();

		Buffer.resize(FileSize);
		File.read((char*)&(Buffer[0]), FileSize);
		File.close();

		return true;
	}
}