#include "ODengine/Timing.h"

#include <algorithm>
OD::FpsLimiter::FpsLimiter()
{

}

void OD::FpsLimiter::Init(float TargetFPS)
{
	SetTargetFPS(TargetFPS);
}

void OD::FpsLimiter::SetTargetFPS(float TargetFPS)
{
	MaxFPS = TargetFPS;
}

void OD::FpsLimiter::Begin()
{
	StartTicks = SDL_GetTicks();
}

float OD::FpsLimiter::End()
{
	CalculateFPS();

	float EndTicks = SDL_GetTicks() - StartTicks;
	//Limit FPS to max fps.
	float TargetMS = 1000.0f / MaxFPS;
	if (TargetMS > EndTicks)
	{
		SDL_Delay((Uint32)(TargetMS - EndTicks));
	}

	return FPS;
}

void OD::FpsLimiter::CalculateFPS()
{
	static const int NUM_SAMPLES = 8;
	static float frameTimes[NUM_SAMPLES];
	static int CurrFrame = 0;

	static float PrevTicks = (float)SDL_GetTicks();
	float CurrTicks;
	CurrTicks = (float)SDL_GetTicks();

	FrameTime = CurrTicks - PrevTicks;
	PrevTicks = CurrTicks;
	frameTimes[CurrFrame % NUM_SAMPLES] = FrameTime;

	int AverageCount;

	++CurrFrame;
	if (CurrFrame < NUM_SAMPLES)
	{
		AverageCount = CurrFrame;
	}
	else
	{
		AverageCount = NUM_SAMPLES;
	}

	float FrameTimeAverage = 0;
	for (int i = 0; i < AverageCount; ++i)
	{
		FrameTimeAverage += frameTimes[i];
	}
	FrameTimeAverage /= AverageCount;

	if (FrameTimeAverage > 0)
	{
		FPS = 1000.0f / FrameTimeAverage;
	}
	else
	{
		FPS = 60.0f;
	}
}

OD::DeltaTime::DeltaTime()
{
}

void OD::DeltaTime::Init(float PhysicsIterations, float DesiredFPS)
{
	PHYS_ITERATIONS = PhysicsIterations;
	DESIRED_FPS = DesiredFPS;

	DESIRED_FRAMETIME = MS_PER_SECOND / DESIRED_FPS;
}

void OD::DeltaTime::Begin()
{
	pTicks = SDL_GetTicks();

	
}

void OD::DeltaTime::Calculate()
{
	nTicks = SDL_GetTicks();
	fTime = nTicks - pTicks;
	pTicks = nTicks;
	IterCounter = 0;
	TotalDTime = fTime / DESIRED_FRAMETIME;
}

bool OD::DeltaTime::Iterate()
{
	if (TotalDTime > 0.0f)
	{
		if (IterCounter < PHYS_ITERATIONS)
		{
			DeltaSeconds = std::min(TotalDTime, MAX_DELTA);
			return true;
		}
	}
	return false;
}

void OD::DeltaTime::End()
{
	TotalDTime -= DeltaSeconds;
	++IterCounter;
}
