#include "ODengine/ImageLoader.h"

#include "ODengine/picoPNG.h"
#include "ODengine/IO_Manager.h"
#include "ODengine/ErrorHandling.h"

#include <string>
namespace OD
{
	GL_Texture ImageLoader::loadPNG(const char * FilePath)
	{
		GL_Texture Texture = {};

		std::vector<unsigned char> In;
		std::vector<unsigned char> Out;
		unsigned long Width, Height;
		if (IO_Manager::LoadFileToBuffer(FilePath, In) == false)
		{
			fatalError("Failed To Load PNG File To Buffer!");
		}
		int ErrorCode = decodePNG(Out, Width, Height, &(In[0]), In.size());
		if (ErrorCode != 0)
		{
			fatalError("decodePNG Failed With Error: " + std::to_string(ErrorCode));
		}

		glGenTextures(1, &(Texture.ID));
		glBindTexture(GL_TEXTURE_2D, Texture.ID);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(Out[0]));

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);

		glBindTexture(GL_TEXTURE_2D, 0);

		Texture.Size.x = (float)Width;
		Texture.Size.y = (float)Height;

		return Texture;
	}
}