#pragma once

#include <GLM/glm.hpp>
#include <GLEW/glew.h>
namespace OD
{
	struct GL_Texture
	{
		GLuint ID;
		glm::vec2 Size;
	};
}