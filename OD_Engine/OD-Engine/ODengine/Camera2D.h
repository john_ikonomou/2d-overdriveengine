#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>

namespace OD
{
	class Camera2D
	{
	public:
		Camera2D();
		~Camera2D();

		void Init(const glm::vec2& ScreenDimensions);
		void Update();

		glm::vec2 ConvertScreenToWorld(glm::vec2 ScreenCoords);
		bool IsBoxInView(const glm::vec2& Pos, const glm::vec2 Size);

		void SetPosition(const glm::vec2& NewPos) { m_Position = NewPos; m_NeedsMatrixUpdate = true; }
		const glm::vec2 GetPosition() { return m_Position; }

		void SetScale(const float& NewScale) { m_Scale = NewScale; m_NeedsMatrixUpdate = true; }
		const float GetScale() { return m_Scale; }

		void SetSpeed(const float& NewSpeed) { m_CameraSpeed = NewSpeed;}
		const float GetSpeed() { return m_CameraSpeed; }

		//void SetCameraMatrix(const glm::mat4& NewCMatrix) { m_CameraMatrix = NewCMatrix; }
		const glm::mat4 GetCameraMatrix() { return m_CameraMatrix; }

	private:
		bool m_NeedsMatrixUpdate;
		glm::vec2 m_Position;
		glm::mat4 m_CameraMatrix;
		glm::mat4 m_orthoMatrix;
		float m_Scale;
		float m_CameraSpeed;

		glm::vec2 m_ScreenDimensions;
	};
}
