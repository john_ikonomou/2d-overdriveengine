#pragma once

#include <GLEW/glew.h>
#include <SDL/SDL.h>

#include <GLM/glm.hpp>

enum WindowFlags
{
	HIDDEN     = 0x1,
	FULLSCREEN = 0x2,
	BORDERLESS = 0x4
};
namespace OD
{
	class Window
	{
	public:
		Window();
		~Window();

		int Create(char* WindowName, const glm::vec2& ScreenDimensions, unsigned int CurrentFlags);

		void SwapBuffers();

		glm::vec2 GetScreenDimensions() { return m_ScreenDimensions; }
		void SetScreenDimensions(glm::vec2 Value) { m_ScreenDimensions = Value; }
	private:
		SDL_Window* m_Window;
		glm::vec2 m_ScreenDimensions;
	};
}
