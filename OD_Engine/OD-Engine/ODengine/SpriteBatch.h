#pragma once

#include <GLEW/glew.h>
#include <GLM/glm.hpp>

#include <vector>

#include "ODengine/Vertex.h"
#include "ODengine/GL_Texture.h"

namespace OD
{
enum class GlyphSortType
{
	NONE,
	FRONT_TO_BACK,
	BACK_TO_FRONT,
	TEXTURE
};

class Glyph
{
public:
	Glyph() {};
	Glyph(const glm::vec4 & DestRect, const glm::vec4 & uvRect, GLuint Texture, float aDepth, const glm::vec4 & Color)
	{
		TextureID = Texture;
		Depth = aDepth;

		TopLeft.Color = Color;
		TopLeft.SetPosition(DestRect.x - DestRect.z / 2.0f, DestRect.y + DestRect.w / 2.0f);
		TopLeft.SetUVs(uvRect.x, uvRect.y + uvRect.w);

		BotLeft.Color = Color;
		BotLeft.SetPosition(DestRect.x - DestRect.z / 2.0f, DestRect.y - DestRect.w / 2.0f);
		BotLeft.SetUVs(uvRect.x, uvRect.y);

		BotRight.Color = Color;
		BotRight.SetPosition(DestRect.x + DestRect.z / 2.0f, DestRect.y - DestRect.w / 2.0f);
		BotRight.SetUVs(uvRect.x + uvRect.z, uvRect.y);

		TopRight.Color = Color;
		TopRight.SetPosition(DestRect.x + DestRect.z / 2.0f, DestRect.y + DestRect.w / 2.0f);
		TopRight.SetUVs(uvRect.x + uvRect.z, uvRect.y + uvRect.w);
	}
	Vertex TopLeft;
	Vertex BotLeft;
	Vertex TopRight;
	Vertex BotRight;

	GLuint TextureID;
	float Depth;
};

class RenderBatch
{
public:
	RenderBatch(GLuint aOffset, GLuint anVerticies, GLuint aTexture) :
		Offset(aOffset),
		nVerticies(anVerticies),
		Texture(aTexture)
	{

	}
	GLuint Offset;
	GLuint nVerticies;
	GLuint Texture;
};

class SpriteBatch
{
public:
	SpriteBatch();
	~SpriteBatch();

	void Init();

	void Begin(GlyphSortType SortType = GlyphSortType::TEXTURE);
	void Draw(const glm::vec4& DestRect, const glm::vec4& uvRect, GLuint Texture, float Depth, const glm::vec4& Color);
	void End();

	void Render_Batch();
private:
	void CreateRenderBatches();
	void CreateVertexArray();
	void SortGlyphs();

	static bool CompareF2B(Glyph* A, Glyph* B);
	static bool CompareB2F(Glyph* A, Glyph* B);
	static bool CompareTEX(Glyph* A, Glyph* B);

	GLuint VBO;
	GLuint VAO;

	std::vector<Glyph*> GlyphPTR; //For Sorting
	std::vector<Glyph> Glyphs;
	GlyphSortType gSortType;

	std::vector<RenderBatch> RenderBatches;
};
}