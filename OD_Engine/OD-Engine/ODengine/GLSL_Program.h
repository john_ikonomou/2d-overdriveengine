#pragma once
#include <string>
#include <GLEW/glew.h>
#include "ODengine/Camera2D.h"

namespace OD
{
	class GLSL_Program
	{
	public:
		GLSL_Program();
		~GLSL_Program();

		void CompileShaders(const std::string& VertexShaderFilePath, const std::string& FragmentShaderFilePath);
		void LinkShaders();

		void AddAttribute(const std::string& AttributeName);

		GLint GetUniformLocation(const std::string UniformName);
		void UseCameraMatrix(OD::Camera2D* RenderCamera);

		void Use();
		void UnUse();
	private:
		GLuint NumAttributes;

		char* LoadFileToString(const char* FilePath);
		void CompileShader(const char* FilePath, GLuint ID);

		GLuint programID;

		GLuint vertShaderID;
		GLuint fragShaderID;
	};
}
