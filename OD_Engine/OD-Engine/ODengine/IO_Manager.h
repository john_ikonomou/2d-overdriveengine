#pragma once
#include <vector>
#include <string>

namespace OD
{
	class IO_Manager
	{
	public:
		static char* LoadFileToString(const char* FilePath);
		static bool LoadFileToBuffer(const char* FilePath, std::vector<unsigned char>& Buffer);
	};
}
