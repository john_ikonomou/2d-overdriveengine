#pragma once

#include "GL_Texture.h"
#include <map>

namespace OD
{
	class TextureCache
	{
	public:
		TextureCache();
		~TextureCache();

		GL_Texture GetTexture(std::string TextureName);
	private:
		std::map<std::string, GL_Texture> TextureMap;
	};
}
