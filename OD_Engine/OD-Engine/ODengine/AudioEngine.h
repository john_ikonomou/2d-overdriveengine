#pragma once

#include <SDL_Mixer/SDL_mixer.h>
#include <map>
namespace OD
{
	class SoundEffect
	{
	public:
		friend class AudioEngine;

		//if Loops = -1, Loop forever
		void Play(int Loops = 0);

	private:
		Mix_Chunk* m_Chunk = nullptr;
	};

	class Music
	{
	public:
		friend class AudioEngine;

		//if Loops = -1, Loop forever
		void Play(int Loops = 1);
		static void Stop();
		static void Pause();
		static void Resume();
	private:
		Mix_Music* m_Music = nullptr;
	};

	class AudioEngine
	{
	public:
		AudioEngine();
		~AudioEngine();

		void Init();
		void Destroy();

		SoundEffect LoadSoundEffect(const char* FilePath);
		Music LoadMusic(const char* FilePath);

	private:
		bool m_IsInitialized = false;
		std::map<std::string, Mix_Chunk*> m_SFX_Cache;
		std::map<std::string, Mix_Music*> m_Music_Cache;
	};

}