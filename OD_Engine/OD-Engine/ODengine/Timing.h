#pragma once

#include <SDL/SDL.h>

namespace OD
{
	//FPS LIMITER CLASS
class FpsLimiter
{
public:
	FpsLimiter();

	void Init(float TargetFPS);
	void SetTargetFPS(float TargetFPS);

	void Begin();
	float End(); //Will return FPS
private:
	void CalculateFPS();

	float MaxFPS;
	float FPS;
	float FrameTime;

	unsigned int EndTicks;
	unsigned int StartTicks;
};
	//DeltaTime HELPER CLASS
class DeltaTime
{
public:
	DeltaTime();

	void Init(float PhysicsIterations = 6.f, float DesiredFPS = 60.f);
	void Begin();
	void Calculate();
	bool Iterate();
	void End();

	const float Delta() { return DeltaSeconds; }
	const float Ticks() { return SDL_GetTicks(); }
private:
	const float MS_PER_SECOND = 1000.0f;
	const float MAX_DELTA = 1.0f;
	float DESIRED_FRAMETIME;
	float DESIRED_FPS;
	float PHYS_ITERATIONS;

	float pTicks;
	float nTicks;
	float fTime;
	float TotalDTime;

	int IterCounter;
	

	float DeltaSeconds;
};

}