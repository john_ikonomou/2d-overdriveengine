#pragma once

#include "GL_Texture.h"
#include "TextureCache.h"
namespace OD
{
	class ResourceManager
	{
	public:
		static GL_Texture GetTexture(std::string TexturePath);

	private:
		static TextureCache TextureC;
	};
}
