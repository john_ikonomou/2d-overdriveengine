#pragma once

#include <GLEW/glew.h>
#include <GLM/glm.hpp>
#include "GL_Texture.h"
#include "ResourceManager.h"

#include <string>
namespace OD
{
	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		void Init(float X, float Y, float W, float H, char* TexturePath);
		void Init(glm::vec2 aPosition, glm::vec2 aSize, char* TexturePath);
		//void Init(float X, float Y, char* TexturePath);
		//void Init(glm::vec2 aPosition, char* TexturePath);

		void Draw();


		glm::vec2 Position;
		glm::vec2 Size;
	private:
		GL_Texture Texture;
		std::string TexturePath;

		GLuint vboID;
	};
}
