#pragma once

#include "GL_Texture.h"
namespace OD
{
	class ImageLoader
	{
	public:
		static GL_Texture loadPNG(const char* FilePath);
	};
}
