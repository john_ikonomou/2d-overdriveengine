#pragma once
#include <unordered_map>
#include <GLM/glm.hpp>
namespace OD
{
class InputManager
{
public:
	InputManager();
	~InputManager();

	void Update();

	void PressKey(unsigned int KeyID);
	void ReleaseKey(unsigned int KeyID);

	bool IsKeyDown(unsigned int KeyID);
	bool IsKeyPressed(unsigned int KeyID); 
	bool IsKeyReleased(unsigned int KeyID);

	void SetMouseCoords(glm::vec2 a_mCoords);
	void SetMouseCoords(float MouseX, float MouseY);

	//Getter for the mouse
	glm::vec2 GetMouseCoords() const { return MouseCoords; }
private:
	bool WasKeyDown(unsigned int KeyID);

	std::unordered_map<unsigned int, bool> m_KeyMap;
	std::unordered_map<unsigned int, bool> m_PrevKeyMap;
	glm::vec2 MouseCoords;
};
}
