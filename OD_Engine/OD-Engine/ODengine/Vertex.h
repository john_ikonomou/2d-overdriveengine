#pragma once

#include <GLEW/glew.h>
#include <GLM/glm.hpp>
namespace OD
{
	struct Vertex
	{
		glm::vec2 Position;
		void SetPosition(glm::vec2 aPos) { Position = aPos; }
		void SetPosition(float X, float Y) { Position = glm::vec2(X, Y); }

		glm::vec4 Color;
		void SetColour(glm::vec4 aColor) { Color = aColor; }
		void SetColour(float R, float G, float B, float A) { Color = glm::vec4(R, G, B, A); }

		glm::vec2 UV;
		void SetUVs(glm::vec2 UVcoords) { UV = UVcoords; }
		void SetUVs(float U, float V) { UV = glm::vec2(U, V); }
	};
}