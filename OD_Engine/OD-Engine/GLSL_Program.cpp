
#include "ODengine/GLSL_Program.h"
#include "ODengine/ErrorHandling.h"

#include <GLM/glm.hpp>
#include <vector>
#include <iostream>

OD::GLSL_Program::GLSL_Program() : programID(0), vertShaderID(0), fragShaderID(0), NumAttributes(0)
{
}


OD::GLSL_Program::~GLSL_Program()
{
}

void OD::GLSL_Program::CompileShaders(const std::string& VertexShaderFilePath, const std::string& FragmentShaderFilePath)
{
	programID = glCreateProgram();

	//Vertex Shader
	vertShaderID = glCreateShader(GL_VERTEX_SHADER);
	if (vertShaderID == 0)
	{
		fatalError("Vertex Shader Failed To Create!");
	}
	CompileShader(VertexShaderFilePath.c_str(), vertShaderID);

	//Fragment Shader
	fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	if (fragShaderID == 0)
	{
		fatalError("Fragment Shader Failed To Create!");
	}
	CompileShader(FragmentShaderFilePath.c_str(), fragShaderID);
}

void OD::GLSL_Program::LinkShaders()
{
	//Vertex and fragment shaders are successfully compiled.
	//Now time to link them together into a program.

	//Attach our shaders to our program
	glAttachShader(programID, vertShaderID);
	glAttachShader(programID, fragShaderID);

	//Link our program
	glLinkProgram(programID);

	//Error Chacking
	//Note the different functions here: glGetProgram* instead of glGetShader*.
	GLint isLinked = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(programID, maxLength, &maxLength, &infoLog[0]);

		//We don't need the program anymore.
		glDeleteProgram(programID);

		std::cout << &infoLog[0] << std::endl;

		fatalError("Failed To Link Shader Program");
	}

	//Always detach shaders after a successful link.
	glDetachShader(programID, vertShaderID);
	glDetachShader(programID, fragShaderID);
	//Don't leak shaders either.
	glDeleteShader(vertShaderID);
	glDeleteShader(fragShaderID);
}

void OD::GLSL_Program::AddAttribute(const std::string& AttributeName)
{
	glBindAttribLocation(programID, NumAttributes, AttributeName.c_str());
	++NumAttributes;
}

GLint OD::GLSL_Program::GetUniformLocation(const std::string UniformName)
{
	GLuint Location = glGetUniformLocation(programID, UniformName.c_str());
	if (Location == GL_INVALID_INDEX)
	{
		fatalError("Uniform: " + UniformName + ". Not Found In Shader");
	}

	return Location;
}

void OD::GLSL_Program::UseCameraMatrix(OD::Camera2D* RenderCamera)
{
	//Set the Camera Matrix
	GLuint MatrixLocation = GetUniformLocation("P");
	glm::mat4 CameraMatrix = RenderCamera->GetCameraMatrix();
	glUniformMatrix4fv(MatrixLocation, 1, GL_FALSE, &(CameraMatrix[0][0]));
}

void OD::GLSL_Program::Use()
{
	glUseProgram(programID);
	for (GLuint i = 0; i < NumAttributes; ++i)
	{
		glEnableVertexAttribArray(i);
	}
}

void OD::GLSL_Program::UnUse()
{
	glUseProgram(0);
	for (GLuint i = 0; i < NumAttributes; ++i)
	{
		glDisableVertexAttribArray(i);
	}
}

char* OD::GLSL_Program::LoadFileToString(const char * FilePath)
{
	FILE* file;
	fopen_s(&file, FilePath, "rb");
	if (file == nullptr)
	{
		fatalError("File Failed To Load");
		return nullptr;
	}

	fseek(file, 0, SEEK_END);
	unsigned int FileLength = ftell(file);
	fseek(file, 0, SEEK_SET);
	char* Source = new char[FileLength + 1];
	memset(Source, 0, FileLength + 1);
	fread(Source, sizeof(char), FileLength, file);
	fclose(file);

	return Source;
}

void OD::GLSL_Program::CompileShader(const char * FilePath, GLuint ID)
{
	char* ShaderFile = LoadFileToString(FilePath);

	glShaderSource(ID, 1, &ShaderFile, nullptr);
	glCompileShader(ID);
	GLint isCompiled = 0;
	glGetShaderiv(ID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(ID, maxLength, &maxLength, &errorLog[0]);

		std::cout << &errorLog[0] << std::endl;

		// Exit with failure.
		glDeleteShader(ID); // Don't leak the shader.
		fatalError("Shader " + (std::string)FilePath + " Failed To Compile");
	}

}