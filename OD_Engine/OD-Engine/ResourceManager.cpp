#include "ODengine/ResourceManager.h"
namespace OD
{
	TextureCache ResourceManager::TextureC;

	GL_Texture ResourceManager::GetTexture(std::string TexturePath)
	{
		return TextureC.GetTexture(TexturePath);
	}
}