#include "ODengine/Window.h"
#include "ODengine/ErrorHandling.h"

#include <iostream>
namespace OD
{
	Window::Window()
	{
	}


	Window::~Window()
	{
	}

	int Window::Create(char * WindowName, const glm::vec2& ScreenDimensions, unsigned int CurrentFlags)
	{

		//Set the window flags
		Uint32 Flags = SDL_WINDOW_OPENGL;

		if (CurrentFlags & WindowFlags::HIDDEN)
		{
			Flags |= SDL_WINDOW_HIDDEN;
		}
		if (CurrentFlags & WindowFlags::FULLSCREEN)
		{
			Flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}
		if (CurrentFlags & WindowFlags::BORDERLESS)
		{
			Flags |= SDL_WINDOW_BORDERLESS;
		}

		//Create a window
		int ScreenX = ScreenDimensions.x;
		int ScreenY = ScreenDimensions.y;

		m_Window = SDL_CreateWindow(WindowName,
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			ScreenX,
			ScreenY,
			Flags);
		if (m_Window == nullptr)
		{
			OD::fatalError("SDL Window Failed To Create!");
		}

		SDL_GLContext GLContext = SDL_GL_CreateContext(m_Window);
		if (GLContext == nullptr)
		{
			OD::fatalError("SDL_GL Context Failed To Create!");
		}

		GLenum error = glewInit();
		if (error != GLEW_OK)
		{
			OD::fatalError("Failed To Initialize GLEW!");
		}

		//Set VSYNC
		SDL_GL_SetSwapInterval(false);

		std::cout << "*** OpenGL VERSION: " << glGetString(GL_VERSION) << " ***" << std::endl;

		//Set the background colour
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);


		//Enable Alpha Blending
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		return 0;
	}

	void Window::SwapBuffers()
	{
		SDL_GL_SwapWindow(m_Window);
	}
}