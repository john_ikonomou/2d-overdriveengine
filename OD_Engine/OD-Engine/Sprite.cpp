#include <cstddef>

#include "ODengine/Sprite.h"

#include "ODengine/Vertex.h"

namespace OD
{

	Sprite::Sprite() : vboID(0), Position(glm::vec2(0)), Size(glm::vec2(0))
	{
	}


	Sprite::~Sprite()
	{
		if (vboID != 0)
		{
			glDeleteBuffers(1, &vboID);
		}
	}

	void Sprite::Init(float X, float Y, float W, float H, char* TexturePath)
	{
		Init(glm::vec2(X, Y), glm::vec2(W, H), TexturePath);
	}

	void Sprite::Init(glm::vec2 aPosition, glm::vec2 aSize, char* TexturePath)
	{
		Position = aPosition;
		Size = aSize;

		Texture = ResourceManager::GetTexture(TexturePath);

		if (vboID == 0)
		{
			glGenBuffers(1, &vboID);
		}

		Vertex VertexData[6];
		//First Triangle
		VertexData[0].SetPosition(aPosition.x + (aSize.x / 2.0f), aPosition.y + (aSize.y / 2.0f));
		VertexData[0].SetUVs(1.0f, 1.0f);					   					
		VertexData[1].SetPosition(aPosition.x - (aSize.x / 2.0f), aPosition.y + (aSize.y / 2.0f));
		VertexData[1].SetUVs(0.0f, 1.0f);					   					
		VertexData[2].SetPosition(aPosition.x - (aSize.x / 2.0f), aPosition.y - (aSize.y / 2.0f));
		VertexData[2].SetUVs(0.0f, 0.0f);					   					
															   					
		//Second Triangle									   					
		VertexData[3].SetPosition(aPosition.x - (aSize.x / 2.0f), aPosition.y - (aSize.y / 2.0f));
		VertexData[3].SetUVs(0.0f, 0.0f);					   					
		VertexData[4].SetPosition(aPosition.x + (aSize.x / 2.0f), aPosition.y - (aSize.y / 2.0f));
		VertexData[4].SetUVs(1.0f, 0.0f);					   					
		VertexData[5].SetPosition(aPosition.x + (aSize.x / 2.0f), aPosition.y + (aSize.y / 2.0f));
		VertexData[5].SetUVs(1.0f, 1.0f);

		for (int i = 0; i < 6; ++i)
		{
			VertexData[i].SetColour(1, 1, 1, 1);
		}

		//Bind buffers with vertex data
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData), VertexData, GL_DYNAMIC_DRAW);

		//Unbind for code safety
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}


	void Sprite::Draw()
	{
		glBindTexture(GL_TEXTURE_2D, Texture.ID);

		//Bind the buffer object to OGL
		glBindBuffer(GL_ARRAY_BUFFER, vboID);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		//Unbind for code safety
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}