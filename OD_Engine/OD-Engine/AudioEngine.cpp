#include "ODengine/AudioEngine.h"
#include "ODengine/ErrorHandling.h"
#include <MAKRON/Debug.h>

OD::AudioEngine::AudioEngine()
{
}


OD::AudioEngine::~AudioEngine()
{
	Destroy();
}

void OD::AudioEngine::Init()
{
	if (m_IsInitialized)
	{
		fatalError("Audio ReInitilization!");
	}
	//Parameter can be a bitwise combination of:
	//MIX_INIT_MOD, MIX_INIT_MP3, MIX_INIT_FLAC, MIX_INIT_OGG
	
	if (Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG | MIX_INIT_FLAC) == -1)
	{
		fatalError("Mix_Init Error: " + std::string(Mix_GetError()));
	}
	/*
	Initialize the mixer API
	-Signed 16-bit Samples, Little-Endian Byte Order
	-44100hz Frequency (CD Audio)
	-2 Channels
	-Chunksize = 2048 
	*/
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048) == -1)
	{
		fatalError("Mix_OpenAudio Error: " + std::string(Mix_GetError()));
	}
	m_IsInitialized = true;
}

void OD::AudioEngine::Destroy()
{
	if (m_IsInitialized)
	{
		m_IsInitialized = false;

		//Free up sound effects
		for (auto& it : m_SFX_Cache)
		{
			Mix_FreeChunk(it.second);
		}
		m_SFX_Cache.clear();

		//Free up music
		for (auto& it : m_Music_Cache)
		{
			Mix_FreeMusic(it.second);
		}
		m_Music_Cache.clear();

		//Call Quit Functions
		Mix_CloseAudio();
		Mix_Quit();
	}
}

OD::SoundEffect OD::AudioEngine::LoadSoundEffect(const char * FilePath)
{
	//Create a sound effect
	SoundEffect Effect;

	//Try to find the audio
	auto iter = m_SFX_Cache.find(FilePath);
	if (iter == m_SFX_Cache.end())
	{
		//Failed to find the SFX, Must load to cache
		Mix_Chunk* chunk = Mix_LoadWAV(FilePath);
		//Check for errors
		if (chunk == nullptr)
		{
			fatalError("Mix_LoadWAV Error: " + std::string(Mix_GetError()));
		}
		m_SFX_Cache[FilePath] = chunk;

	}
	else
	{
		//It is already cacheds
		Effect.m_Chunk = iter->second;
	}

	//If we get this far, return
	return Effect;
}

OD::Music OD::AudioEngine::LoadMusic(const char * FilePath)
{
	//Create a sound effect
	Music music;

	//Try to find the audio
	auto iter = m_Music_Cache.find(FilePath);
	if (iter == m_Music_Cache.end())
	{
		//Failed to find the SFX, Must load to cache
		Mix_Music* MixMus = Mix_LoadMUS(FilePath);
		//Check for errors
		if (MixMus == nullptr)
		{
			fatalError("Mix_LoadWAV Error: " + std::string(Mix_GetError()));
		}
		m_Music_Cache[FilePath] = MixMus;
		Debug::Log("Sound File Cached: " + std::string(FilePath));
		music.m_Music = MixMus;
	}
	else
	{
		//It is already cacheds
		music.m_Music = iter->second;
	}

	//If we get this far, return
	return music;
}

void OD::SoundEffect::Play(int Loops)
{
	if (Mix_PlayChannel(-1, m_Chunk, Loops) == -1)
	{
		if (Mix_PlayChannel(0, m_Chunk, Loops) == -1)
		{
			fatalError("Mix_PlayChannel Error: " + std::string(Mix_GetError()));
		}
	}
}

void OD::Music::Play(int Loops)
{
	if(Mix_PlayMusic(m_Music, Loops) == -1)
	{
		fatalError("Mix_PlayMusic Error: " + std::string(Mix_GetError()));
	}
}

void OD::Music::Stop()
{
	Mix_HaltMusic();
}

void OD::Music::Pause()
{
	Mix_PauseMusic();
}

void OD::Music::Resume()
{
	Mix_ResumeMusic();
}
