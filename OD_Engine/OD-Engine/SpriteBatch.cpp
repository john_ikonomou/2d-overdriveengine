#include "ODengine/SpriteBatch.h"

#include <algorithm>

OD::SpriteBatch::SpriteBatch() : VBO(0), VAO(0)
{
}


OD::SpriteBatch::~SpriteBatch()
{
}

void OD::SpriteBatch::Init()
{
	CreateVertexArray();


}

void OD::SpriteBatch::Begin(GlyphSortType SortType/* = GlyphSortType::TEXTURE*/)
{
	gSortType = SortType;
	RenderBatches.clear();

	Glyphs.clear();
}

void OD::SpriteBatch::Draw(const glm::vec4 & DestRect, const glm::vec4 & uvRect, GLuint Texture, float Depth, const glm::vec4 & Color)
{
	Glyphs.emplace_back(DestRect, uvRect, Texture, Depth, Color);
}

void OD::SpriteBatch::End()
{
	GlyphPTR.resize(Glyphs.size());
	for (int i = 0; i < Glyphs.size(); ++i)
	{
		GlyphPTR[i] = &Glyphs[i];
	}

	SortGlyphs();
	CreateRenderBatches();
}

void OD::SpriteBatch::Render_Batch()
{
	glBindVertexArray(VAO);

	for (int i = 0; i < RenderBatches.size(); ++i)
	{
		glBindTexture(GL_TEXTURE_2D, RenderBatches[i].Texture);

		glDrawArrays(GL_TRIANGLES, RenderBatches[i].Offset, RenderBatches[i].nVerticies);
	}

	glBindVertexArray(0);
}

void OD::SpriteBatch::CreateRenderBatches()
{
	std::vector<Vertex> Verticies;
	Verticies.resize(Glyphs.size() * 6);
	if (Glyphs.empty())
	{
		return;
	}

	int Offset = 0;

	int CurrentVert = 0;

	//Add a renderbatch to the vector with the following arguments
	RenderBatches.emplace_back(0, 6, GlyphPTR[0]->TextureID);

	Verticies[CurrentVert++] = GlyphPTR[0]->TopLeft;
	Verticies[CurrentVert++] = GlyphPTR[0]->BotLeft;
	Verticies[CurrentVert++] = GlyphPTR[0]->BotRight;
	Verticies[CurrentVert++] = GlyphPTR[0]->BotRight;
	Verticies[CurrentVert++] = GlyphPTR[0]->TopRight;
	Verticies[CurrentVert++] = GlyphPTR[0]->TopLeft;
	Offset += 6;

	int CurrentGlyph = 1;
	for (; CurrentGlyph < GlyphPTR.size(); ++CurrentGlyph)
	{
		if (GlyphPTR[CurrentGlyph]->TextureID != GlyphPTR[CurrentGlyph - 1]->TextureID)
		{
			RenderBatches.emplace_back(Offset, 6, GlyphPTR[CurrentGlyph]->TextureID);
		}
		else
		{
			RenderBatches.back().nVerticies += 6;
		}

		Verticies[CurrentVert++] = GlyphPTR[CurrentGlyph]->TopLeft;
		Verticies[CurrentVert++] = GlyphPTR[CurrentGlyph]->BotLeft;
		Verticies[CurrentVert++] = GlyphPTR[CurrentGlyph]->BotRight;
		Verticies[CurrentVert++] = GlyphPTR[CurrentGlyph]->BotRight;
		Verticies[CurrentVert++] = GlyphPTR[CurrentGlyph]->TopRight;
		Verticies[CurrentVert++] = GlyphPTR[CurrentGlyph]->TopLeft;
		Offset += 6;
	}

	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, Verticies.size() * sizeof(Vertex), nullptr, GL_DYNAMIC_DRAW); //Orphan the buffer
	glBufferSubData(GL_ARRAY_BUFFER, 0, Verticies.size() * sizeof(Vertex), Verticies.data());
}

void OD::SpriteBatch::CreateVertexArray()
{
	//Generate VAO
	if (VAO == 0)
	{
		glGenVertexArrays(1, &VAO);
	}
	glBindVertexArray(VAO);

	if (VBO == 0)
	{
		glGenBuffers(1, &VBO);
	}
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	//Enable neccecary attribute arrays.
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	//Setup vertex attributes for Position, UV's and Color.
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Color));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, UV));

	//Unbind everything
	glBindVertexArray(0);
}

void OD::SpriteBatch::SortGlyphs()
{
	switch (gSortType)
	{
	case GlyphSortType::BACK_TO_FRONT:
		std::stable_sort(GlyphPTR.begin(), GlyphPTR.end(), CompareB2F);
			break;
	case GlyphSortType::FRONT_TO_BACK:
		std::stable_sort(GlyphPTR.begin(), GlyphPTR.end(), CompareF2B);
			break;
	case GlyphSortType::TEXTURE:
		std::stable_sort(GlyphPTR.begin(), GlyphPTR.end(), CompareTEX);
			break;
	}
}

bool OD::SpriteBatch::CompareF2B(Glyph * A, Glyph * B)
{
	return (A->Depth < B->Depth);
}
bool OD::SpriteBatch::CompareB2F(Glyph * A, Glyph * B)
{
	return (A->Depth > B->Depth);
}
bool OD::SpriteBatch::CompareTEX(Glyph * A, Glyph * B)
{
	return (A->TextureID < B->TextureID);
}