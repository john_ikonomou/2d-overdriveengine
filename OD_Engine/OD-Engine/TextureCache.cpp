#include "ODengine/TextureCache.h"
#include "ODengine/ImageLoader.h"

#include <iostream>
namespace OD
{
	TextureCache::TextureCache()
	{
	}


	TextureCache::~TextureCache()
	{
	}

	GL_Texture TextureCache::GetTexture(std::string TexturePath)
	{
		//Lookup the texture to see if it in the map
		auto TexIterator = TextureMap.find(TexturePath);
		if (TexIterator == TextureMap.end())
		{
			GL_Texture newTexture = ImageLoader::loadPNG(TexturePath.c_str());
			TextureMap.insert(make_pair(TexturePath, newTexture));

			std::cout << "Texture " << TexturePath.c_str() << " Loaded Correctly!" << std::endl;
			std::cout << "Loaded New Texture!" << std::endl;

			return newTexture;
		}

		std::cout << "Texture " << TexturePath.c_str() << " Loaded Correctly!" << std::endl;
		std::cout << "Used Cache Texture!" << std::endl;
		return TexIterator->second;
	}
}