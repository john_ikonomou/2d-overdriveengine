#include "ODengine/ErrorHandling.h"

#include <iostream>
#include <SDL/SDL.h>

namespace OD
{
	void fatalError(std::string errorString)
	{
		std::cout << errorString << std::endl;

		system("pause");

		SDL_Quit();
		exit(69);
	}
}