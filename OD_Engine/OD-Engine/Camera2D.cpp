#include "ODengine/Camera2D.h"

OD::Camera2D::Camera2D() :
	m_Position(0.0f), 
	m_CameraMatrix(1.0f),
	m_orthoMatrix(1.0f),
	m_Scale(1.0f),
	m_CameraSpeed(2.0f),
	m_NeedsMatrixUpdate(true),
	m_ScreenDimensions(glm::vec2(640.0f, 480.0f))
{
}

OD::Camera2D::~Camera2D()
{

}

void OD::Camera2D::Init(const glm::vec2 & ScreenDimensions)
{
	m_ScreenDimensions = ScreenDimensions;
	m_orthoMatrix = glm::ortho(0.0f, m_ScreenDimensions.x,
							   0.0f, m_ScreenDimensions.y);
}

void OD::Camera2D::Update()
{
	if (m_NeedsMatrixUpdate)
	{
		m_CameraMatrix = glm::translate(m_orthoMatrix, glm::vec3(-m_Position.x + (m_ScreenDimensions.x / 2.0f), -m_Position.y + (m_ScreenDimensions.y / 2.0f), 0.0f));
		m_CameraMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(m_Scale, m_Scale, 0.0f)) * m_CameraMatrix;
		
		m_NeedsMatrixUpdate = false;
	}
}
glm::vec2 OD::Camera2D::ConvertScreenToWorld(glm::vec2 ScreenCoords)
{	
	//Make it so the 0,0 is Centre
	ScreenCoords -= glm::vec2(m_ScreenDimensions.x / 2, m_ScreenDimensions.y / 2);
	//Scale it based on the cameras zoom factor
	ScreenCoords /= m_Scale;
	//Translate it based on the cameras translation
	ScreenCoords += m_Position;
	return ScreenCoords;
}
bool OD::Camera2D::IsBoxInView(const glm::vec2 & Pos, const glm::vec2 Size)
{
	glm::vec2 ScaledScreen = m_ScreenDimensions / m_Scale;

	float MinX = Size.x / 2.0f + ScaledScreen.x / 2.0f;
	float MinY = Size.y / 2.0f + ScaledScreen.y / 2.0f;

	//Centre
	glm::vec2 CentrePos = Pos + (Size / 2.0f);
	glm::vec2 CentreCamPos = m_Position;

	//vector from camera to box
	glm::vec2 DistVec = CentrePos - CentreCamPos;

	float xDepth = MinX - abs(DistVec.x);
	float yDepth = MinY - abs(DistVec.y);

	if (xDepth > 0 && yDepth > 0)
	{
		//Collision Occured
		return true;
	}

	return false;
}
