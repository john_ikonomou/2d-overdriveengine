#include "ODengine/InputManager.h"

OD::InputManager::InputManager() : MouseCoords(glm::vec2(0))
{
}

OD::InputManager::~InputManager()
{
}

void OD::InputManager::Update()
{
	for (auto& iter: m_KeyMap)
	{
		m_PrevKeyMap[iter.first] = iter.second;
	}
}

void OD::InputManager::PressKey(unsigned int KeyID)
{
	m_KeyMap[KeyID] = true;
}

void OD::InputManager::ReleaseKey(unsigned int KeyID)
{
	m_KeyMap[KeyID] = false;
}

void OD::InputManager::SetMouseCoords(glm::vec2 a_mCoords)
{
	MouseCoords = a_mCoords;
}

void OD::InputManager::SetMouseCoords(float MouseX, float MouseY)
{
	MouseCoords = glm::vec2(MouseX, MouseY);
}

bool OD::InputManager::IsKeyDown(unsigned int KeyID)
{
	auto iter = m_KeyMap.find(KeyID);
	if (iter != m_KeyMap.end())
	{
		return iter->second;
	}
	return false;
}

bool OD::InputManager::IsKeyPressed(unsigned int KeyID)
{
	//Check if it is pressed this frame and wasnt last frame

	if (IsKeyDown(KeyID) && !WasKeyDown(KeyID))
	{
		return true;
	}
	return false;
}

bool OD::InputManager::IsKeyReleased(unsigned int KeyID)
{
	//Check if its not pressed this frame and was last frame

	if (!IsKeyDown(KeyID) && WasKeyDown(KeyID))
	{
		return true;
	}
	return false;
}

bool OD::InputManager::WasKeyDown(unsigned int KeyID)
{
	auto iter = m_PrevKeyMap.find(KeyID);
	if (iter != m_PrevKeyMap.end())
	{
		return iter->second;
	}
	return false;
}