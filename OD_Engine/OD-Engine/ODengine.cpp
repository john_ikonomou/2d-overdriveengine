#include "ODengine/ODengine.h"

#include <GLEW/glew.h>
#include <SDL/SDL.h>
namespace OD
{
	int Init()
	{
		///Initialize SDL
		SDL_Init(SDL_INIT_EVERYTHING);

		//Enable double buffering
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}
}