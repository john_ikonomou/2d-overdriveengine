#pragma once
#include <GLM/glm.hpp>
#include <ODengine/SpriteBatch.h>
class GameFloor
{
public:
	GameFloor(glm::vec2 Pos);
	~GameFloor();

	void Draw(OD::SpriteBatch& SpriteBatch);

private:
	glm::vec2 Position;
};

