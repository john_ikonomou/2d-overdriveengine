#include "Bullet.h"
#include <ODengine/Sprite.h>

Bullet::Bullet(glm::vec2 Pos, glm::vec2 Dir, float Speed, int FrameLife)
{
	Position = Pos;
	Direction = Dir;
	Velocity = Speed;
	Lifetime = FrameLife;
}

Bullet::~Bullet()
{
}

void Bullet::Draw(OD::SpriteBatch& SpriteBatch)
{
	static OD::GL_Texture Tex = OD::ResourceManager::GetTexture("Data/Textures/Bullet.png");

	//Create a DestRect
	glm::vec4 PosAndSize = glm::vec4(Position, Tex.Size.x, Tex.Size.y);

	SpriteBatch.Draw(PosAndSize,
					 glm::vec4(0.0f, 0.0f, 1.0f, 1.0f),
					 Tex.ID,
					 0.0f,
					 glm::vec4(1, 1, 1, 1));
}

bool Bullet::Update(float DeltaSeconds)
{
	Position += Direction * (Velocity * DeltaSeconds);
	Lifetime--;
	if (Lifetime <= 0)
	{
		return true;
	}
	return false;
}
