#pragma once
#include <GLM/glm.hpp>
#include "ODengine//SpriteBatch.h"
#include "ODengine/InputManager.h"
#include "ODengine/Camera2D.h"
class Player
{
public:
	Player();
	Player(glm::vec2 Pos, OD::InputManager* IM, OD::Camera2D* MainCamera);
	~Player();

	void Draw(OD::SpriteBatch& SpriteBatch);
	void Update(float DeltaSeconds);

	glm::vec2 Direction = glm::vec2(1, 0);
	glm::vec2 Position = glm::vec2(0,0);
	float Speed = 4.0f;
private:

	OD::InputManager* Input;
	OD::Camera2D* Camera;
};

