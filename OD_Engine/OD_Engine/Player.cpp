#include "Player.h"

#include "ODengine/Sprite.h"
#include <SDL/SDL.h>

Player::Player()
{
}

Player::Player(glm::vec2 Pos, OD::InputManager* IM, OD::Camera2D* MainCamera)
{
	Position = Pos;
	Input = IM;
	Camera = MainCamera;
}


Player::~Player()
{
}

void Player::Draw(OD::SpriteBatch & SpriteBatch)
{
	static OD::GL_Texture Tex = OD::ResourceManager::GetTexture("Data/Textures/Player.png");

	//Create a DestRect
	glm::vec4 PosAndSize = glm::vec4(Position, Tex.Size.x, Tex.Size.y);

	SpriteBatch.Draw(PosAndSize,
					 glm::vec4(0.0f, 0.0f, 1.0f, 1.0f),
					 Tex.ID,
					 0,
					 glm::vec4(1, 1, 1, 1));
}

void Player::Update(float DeltaSeconds)
{
	//HACK: This should be done elsewhere
	if (Input->IsKeyDown(SDLK_w))
	{
		Position = Position + glm::vec2(0, Speed * DeltaSeconds);
	}
	if (Input->IsKeyDown(SDLK_a))
	{
		Position = Position + glm::vec2(-Speed * DeltaSeconds, 0);
	}
	if (Input->IsKeyDown(SDLK_s))
	{
		Position = Position + glm::vec2(0, -Speed * DeltaSeconds);
	}
	if (Input->IsKeyDown(SDLK_d))
	{
		Position = Position + glm::vec2(Speed * DeltaSeconds, 0);
	}
	Camera->SetPosition(Position);
}
