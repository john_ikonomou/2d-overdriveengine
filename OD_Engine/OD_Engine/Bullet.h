#pragma once
#include <ODengine/SpriteBatch.h>
#include <GLM/glm.hpp>

class Bullet
{
public:
	Bullet(glm::vec2 Pos, glm::vec2 Dir, float Speed, int FrameLife);
	~Bullet();

	void Draw(OD::SpriteBatch& SpriteBatch); 
	bool Update(float DeltaSeconds);

private:
	float Velocity;
	glm::vec2 Direction;
	glm::vec2 Position;
	int Lifetime;
};

