#include "Floor.h"
#include "ODengine/ResourceManager.h"
GameFloor::GameFloor(glm::vec2 Pos)
{
	Position = Pos;
}

GameFloor::~GameFloor()
{
}

void GameFloor::Draw(OD::SpriteBatch & SpriteBatch)
{
	static OD::GL_Texture Tex = OD::ResourceManager::GetTexture("Data/Textures/TestUV.png");

	//Create a DestRect
	glm::vec4 PosAndSize = glm::vec4(Position, Tex.Size.x, Tex.Size.y);

	SpriteBatch.Draw(PosAndSize,
					 glm::vec4(0.0f, 0.0f, 1.0f, 1.0f),
					 Tex.ID,
					 1,
					 glm::vec4(1, 1, 1, 1));
}
