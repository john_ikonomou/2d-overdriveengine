#include "MainGame.h"

#include <GLEW/glew.h>
#include <iostream>

int main(int argc, char** argv)
{
	MainGame Game;
	Game.Run();

	return 0;
}