#pragma once

//Windows stuff
#include <Windows.h>
//Standard Library
#include <string>
#include <vector>
//Engine Dependencies
#include <GLEW/glew.h>
#include <SDL/SDL.h>
#include <GLM/glm.hpp>
//Engine Classes
#include "ODengine/Window.h"
#include "ODengine/Camera2D.h"
#include "ODengine/GLSL_Program.h"
#include "ODengine/Sprite.h"
#include "ODengine/GL_Texture.h"
#include "ODengine/SpriteBatch.h"
#include "ODengine/InputManager.h"
#include "ODengine/Timing.h"
#include "ODengine/SpriteFont.h"
#include "ODengine/AudioEngine.h"

#include "Bullet.h"
#include "Player.h"
#include "Floor.h"

//Maximum FPS
#define FPS_LIMIT_MAX 120

//Game/Window Name
#define GAME_WINDOW_NAME "OD-GAME"

//Collection of gamestates
enum class GameState
{
	PLAY,
	EXIT
};

class MainGame
{
public:
	//Constructor/Destructor
	MainGame();
	~MainGame();

	void Run(); //Sets everything in motion, will call GameLoop

private:
	void InitSystems(); //Initialize everything, Create the main Window.
	void InitShaders(); //Compile/Link Shaders, and Add Uniforms

	void GameLoop(); //Reoccuring game loop, runs continually until the gamestate is set to EXIT.
					 //Update the camera, process input, call the update loop and draw the game.

	void Update(float DeltaSeconds = 1.0f);
	void ProcessInput(float DeltaSeconds = 1.0f); //Handles SDL Input Events
	void DrawGame(); //Draw stuff to the backbuffer and swap buffers,
	void DrawHUD();

	OD::Window MainWindow; //The main game window
private:

	float FPS; //Current FPS.

	glm::vec2 MainWindowDimensions; //X/Y Dimentions of the screen (eg. 1920x1080)

	GameState CurrentState; //Current Game State (eg. Play, Exit)

	OD::GLSL_Program ColorProgram; //Shader Program

	OD::Camera2D Camera; //Camera
	OD::Camera2D HudCamera; //Camera

	OD::SpriteBatch Batch; //Spritebatch, to cut down on OGL Data Rebinds 
						   //(eg. Draws every instance of a texture before rebinding the next texture)
	OD::SpriteFont* Font;
	OD::SpriteBatch FontBatch;

	OD::InputManager Input; //Input Manager for Handling Input

	OD::FpsLimiter FpsLim; //FPS Calculator/Regulator
	OD::DeltaTime Time;

	OD::AudioEngine Audio;

	std::vector<Bullet> BulletList;
	std::vector<GameFloor> Floors;
	Player gPlayer;
};

