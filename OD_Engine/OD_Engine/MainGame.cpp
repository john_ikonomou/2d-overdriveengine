#include "MainGame.h"
#include "ODengine/ODengine.h"
#include <iostream>


MainGame::MainGame()
{
	MainWindowDimensions.x = 1366;
	MainWindowDimensions.y = 768;

	CurrentState = GameState::PLAY;

	//Initialize the main camera
	Camera.Init(MainWindowDimensions);
	//Initialize the HUD Camera, 
	//and set its position so that 0,0 is the centre of the main window
	HudCamera.Init(MainWindowDimensions);
	HudCamera.SetPosition(MainWindowDimensions / 2.0f);
}

MainGame::~MainGame()
{	
	Audio.Destroy();
}

void MainGame::Run()
{
	InitSystems();

	//Initialize Shaders
	InitShaders();

	GameLoop();
}

void MainGame::InitSystems()
{
	OD::Init();

	MainWindow.Create(GAME_WINDOW_NAME, MainWindowDimensions, WindowFlags::BORDERLESS);
	

	Batch.Init();
	FontBatch.Init();
	Font = new OD::SpriteFont("Data/Fonts/SVBasicManual.ttf", 32);

	FpsLim.Init(FPS_LIMIT_MAX);

	//Create the player here
	gPlayer = Player(glm::vec2(64, 64), &Input, &Camera);

	for (int x = 0; x < 2; ++x)
	{
		for (int y = 0; y < 1; ++y)
		{
			Floors.emplace_back(glm::vec2(x * 1024, y * 1024));
		}
	}

	Audio.Init(); //init must happen AFTER OD:Init()
	//Start the music
	OD::Music BGM = Audio.LoadMusic("Data/Audio/BGM/Plameny.ogg");
	BGM.Play();
	//system("pause");
}

void MainGame::InitShaders()
{
	ColorProgram.CompileShaders("Data/Shaders/Default.vert", "Data/Shaders/Default.frag");
	ColorProgram.AddAttribute("vertPos");
	ColorProgram.AddAttribute("vertCol");
	ColorProgram.AddAttribute("vertUV");
	ColorProgram.LinkShaders();
}

void MainGame::Update(float DeltaSeconds)
{
	//Update the player
	gPlayer.Update(DeltaSeconds);

	//Spawn some bullets
	if (Input.IsKeyReleased(SDL_BUTTON_LEFT))
	{
		//Get Screen Space Mouse Coordinates centred on the middle of the screen.
		glm::vec2 mMouse = Input.GetMouseCoords() -= MainWindowDimensions / 2.0f;
		mMouse.y *= -1; //Flip the Y


		BulletList.emplace_back(gPlayer.Position,		//Position
								glm::normalize(mMouse), //Direction
								3.5f,					//Speed (Scalar)
								480);					//Lifetime
	}

	//Update the bullets
	for (int i = 0; i < BulletList.size();)
	{
		if (BulletList[i].Update(DeltaSeconds) == true)
		{
			BulletList[i] = BulletList.back();
			BulletList.pop_back();
		}
		else
		{
			++i;
		}
	}
}

void MainGame::ProcessInput(float DeltaSeconds)
{
	float SCALE_SPEED = 0.005f;

	SDL_Event sdlEvent;
	while (SDL_PollEvent(&sdlEvent))
	{
		switch (sdlEvent.type)
		{
		case SDL_QUIT:
			CurrentState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			Input.SetMouseCoords(sdlEvent.motion.x, sdlEvent.motion.y);
			break;
		case SDL_MOUSEBUTTONDOWN:
			Input.PressKey(sdlEvent.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			Input.ReleaseKey(sdlEvent.button.button);
			break;
		case SDL_KEYDOWN:
			Input.PressKey(sdlEvent.key.keysym.sym);
			break;
		case SDL_KEYUP:
			Input.ReleaseKey(sdlEvent.key.keysym.sym);
			break;
		}
	}


	if (Input.IsKeyDown(SDLK_q))
	{
		Camera.SetScale(Camera.GetScale() + (SCALE_SPEED * DeltaSeconds));
	}
	if (Input.IsKeyDown(SDLK_e))
	{
		Camera.SetScale(Camera.GetScale() - (SCALE_SPEED * DeltaSeconds));
	}
}

void MainGame::DrawGame()
{
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ColorProgram.Use();

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(ColorProgram.GetUniformLocation("pTexture"), 0);

	//Set the Camera Matrix
	ColorProgram.UseCameraMatrix(&Camera);

	Batch.Begin();
	for (int j = 0; j < Floors.size(); ++j)
	{
		Floors[j].Draw(Batch);
	}
	gPlayer.Draw(Batch);
	for (int i = 0; i < BulletList.size(); ++i)
	{
		BulletList[i].Draw(Batch);
	}
	Batch.End();
	Batch.Render_Batch();

	DrawHUD();

	glBindTexture(GL_TEXTURE_2D, 0);

	ColorProgram.UnUse();

	//Swap Buffers
	MainWindow.SwapBuffers();
}

void MainGame::DrawHUD()
{
	char buffer[256];

	ColorProgram.UseCameraMatrix(&HudCamera);

	FontBatch.Begin();

	sprintf_s(buffer, "FPS: %d", (int)FPS);
	Font->Draw(FontBatch, buffer, glm::vec2(0, 0), glm::vec2(1.0f), 0, glm::vec4(1, 1, 0, 1));

	sprintf_s(buffer, "TIME: %ds", SDL_GetTicks() / 1000);
	Font->Draw(FontBatch, buffer, glm::vec2(0, 32), glm::vec2(1.0f), 0, glm::vec4(1, 1, 0, 1));

	sprintf_s(buffer, "I LOVE YOU, YOUR THE CUTEST GIRL \nON THE FACE OF THE PLANET :D", SDL_GetTicks() / 1000);
	Font->Draw(FontBatch, buffer, glm::vec2(0, 96), glm::vec2(1.0f), 0, glm::vec4(0.7, 0, 1, 1));

	FontBatch.End();
	FontBatch.Render_Batch();
}

void MainGame::GameLoop()
{
	Time.Init(6, 120);
	Time.Begin(); //Begin Delta calculations


	while (CurrentState != GameState::EXIT)
	{
		FpsLim.Begin(); //Calc/Limit FPS.
		Time.Calculate(); //Do the Delta Calculations

		Input.Update(); //Update the input manager

		while (Time.Iterate()) //Iterate the semi fixed timestep
		{
			//vvvvv|| DO STUFF HERE ||vvvvv\\

			ProcessInput(Time.Delta());
			Update(Time.Delta());

			//^^^^^|| DO DTUFF HERE ||^^^^^\\

			Time.End(); //Finish delta calculation and prep for next frame
		}

		Camera.Update(); //Do this after Processing input/Update Function to prevent unneccesary input latency
		HudCamera.Update();
		DrawGame();
		

		FPS = FpsLim.End();  //Calc/Limit FPS.
	}
}

