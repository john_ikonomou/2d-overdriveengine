#version 130

out vec4 fragColor;

in vec4 vCol;
in vec2 vPos;
in vec2 vUV;

uniform sampler2D pTexture;

void main()
{
	vec4 tCol = texture(pTexture, vUV);
	fragColor = tCol * vCol;
}