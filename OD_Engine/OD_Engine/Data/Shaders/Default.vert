#version 130

in vec2 vertPos;
in vec4 vertCol;
in vec2 vertUV;
out vec4 vCol;
out vec2 vPos;
out vec2 vUV;

uniform mat4 P;

void main()
{
	vPos = vertPos;

	vCol = vertCol;

	vUV = vertUV;
	vUV.y *= -1;

	gl_Position.xy = (P * vec4(vertPos,0.0f,1.0f)).xy;
	gl_Position.z = 0.0f;
	gl_Position.w = 1.0f;
}